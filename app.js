angular.module('myApp', ['bm.uiTour'])

.config(['TourConfigProvider', function (TourConfigProvider) {

    TourConfigProvider.set('scrollOffset', 50);

    TourConfigProvider.set('onStart', function () {
        console.log('Started Tour');
    });

    TourConfigProvider.set('onNext', function () {
        console.log('Moving on...');
    });

    TourConfigProvider.set('onEnd', function () {
        console.log('THE END');
    });

}])




  .run(['uiTourService', function (TourService) {
      TourService.createDetachedTour('detachedDemoTour');
      console.log(TourService);
  }])



  .controller( 'myCtrl', [ '$scope', 'uiTourService', function($scope, TourService) {

    myTour = TourService.getTour();
    console.log('myTour: ', myTour);
    $scope.firstName	= "John";
    $scope.lastName= "Doez";

    $scope.startDetached = function () {
        TourService.getTourByName('detachedDemoTour').start();
    }

}]);