angular.module('myApp',['bm.uiTour']).run(['uiTourService',
  function(uiTourService) {
    //setup the tour
    uiTourService.createDetachedTour('general', {
      name: 'general',
      backdrop: true,
      debug: true
    });

    //testing
    var uitour = uiTourService.getTourByName('general');
    console.log(uitour); // Object {_name: undefined, initialized: true}

    //test tour steps
    console.log(uitour._getSteps()); // []

    //test tour start
    uitour.start().then(function(data) {
      //tour start succeeded
    }, function(error) {
      //tour start failed
      console.log(error); // No steps.
    });

  }
]);